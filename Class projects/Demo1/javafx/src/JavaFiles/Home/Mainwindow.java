package JavaFiles.Home;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Mainwindow extends Application {
    Text java = null;
    Text python = null;
    Text txt = null;
    Text title = null;
    
     @Override
    public void start(Stage prStage){
        prStage.setTitle("JAVAfx");
        prStage.setHeight(800);
        prStage.setWidth(1000);
        prStage.setResizable(true);
        prStage.getIcons().add(new Image("main/assets/image/oo.jpeg"));

        title = new Text(10,20,"This is the Title");
        title.setFill(Color.AQUA);
        title.setFont(new Font(50));

        txt = new Text(10,30,"Hello OD");
        txt.setFill(Color.AQUA);
        txt.setFont(new Font(20));

        Text gd = new Text(600,30,"Have A Nice day");
        gd.setFill(Color.AQUA);
        gd.setFont(new Font(30));
        gd.setFont(new Font(35));

        java = new Text(500,100,"java");
        java.setFont(new Font(20));
        java.setFill(Color.AQUA);
        python = new Text(500,150,"Python");
        python.setFont(new Font(20));
        python.setFill(Color.AQUA);
        Text cpp = new  Text(500,200,"cpp");
        cpp.setFont(new Font(20));
        cpp.setFill(Color.AQUA);


        Text web = new Text(50,50,"web");
        web.setFont(new Font(20));
        web.setFill(Color.AQUA);
        Text backend = new Text (50,75,"Backend");
        backend.setFont(new Font(20));
        backend.setFill(Color.AQUA);
        Text app = new  Text(50,100,"app");
        app.setFont(new Font(20));
        app.setFill(Color.AQUA);

      

        VBox vb = new VBox(20,java,python,cpp);
        vb.setLayoutX(400);
        vb.setLayoutX(100);
        

       VBox vb1 = new VBox(20,web,backend,app);
        vb1.setLayoutX(720);
        vb1.setLayoutY(100);
       HBox hb = new HBox(100,vb,vb1);
       
       hb.setAlignment(Pos.CENTER);
       hb.setLayoutX(400);
       hb.setLayoutY(100);


        Group gr = new Group(txt,hb,gd);


       
        Scene sc = new Scene((Parent) gr,450,100);
        sc.setFill(Color.BLACK);
        prStage.setScene(sc);

        prStage.show();
    }
}
