package JavaFiles.Home;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class StudyMaterial extends Application {

    @Override
    public void start(Stage primaryStage) {
        // Create the main border pane
        BorderPane root = new BorderPane();
        root.setPadding(new Insets(15));

        // Create the top label
        Label studyMaterialLabel = new Label("Study material");
        studyMaterialLabel.setStyle("-fx-font-size: 20px; -fx-font-weight: bold;");
        root.setTop(studyMaterialLabel);
        BorderPane.setAlignment(studyMaterialLabel, Pos.CENTER);

        // Create the text area for adding the file
        TextArea fileTextArea = new TextArea("Add your file");
        fileTextArea.setWrapText(true);
        fileTextArea.setEditable(false);
        root.setCenter(fileTextArea);

        // Create the bottom HBox for buttons
        HBox bottomBox = new HBox(10);
        bottomBox.setPadding(new Insets(10));
        bottomBox.setAlignment(Pos.CENTER);

        // Create labels for the buttons
        Label studyMaterialLabel1 = new Label("Study material");
        Label assignmentLabel = new Label("Assignment");
        Label studentLabel = new Label("Student");
        Label messagesLabel = new Label("Messages");

        // Add the labels to the bottom HBox
        bottomBox.getChildren().addAll(studyMaterialLabel1, assignmentLabel, studentLabel, messagesLabel);
        root.setBottom(bottomBox);

        // Create the scene and show the stage
        Scene scene = new Scene(root, 600, 400);
        primaryStage.setTitle("Study Material App");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

   
}