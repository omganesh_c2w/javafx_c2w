package JavaFiles.Home.ImagePage;

import javax.swing.GrayFilter;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage; 

public class ImgAssi extends Application{ 

    Text txt = null;

      @Override
    public void start(Stage imgStage){
        imgStage.setTitle("ImagePage");
        imgStage.setHeight(1000);
        imgStage.setWidth(1000);
        imgStage.setResizable(true);
      imgStage.getIcons().add(new Image("assets/image/camera.jpg"));    
   
    
    
    Image ig = new Image("assets/image/javalogo.jpg");
    ImageView iv = new ImageView(ig);
    iv.setFitWidth(200);
    iv.setPreserveRatio(true);

    Label l1 = new Label("      JAVA");
        l1.setFont(new Font(30));
        l1.setAlignment(Pos.CENTER);
        l1.setPrefHeight(200);

    Image ig1 = new Image("assets/image/pythonlogo.jpg");
    ImageView iv1 = new ImageView(ig1);
    iv1.setFitWidth(200);
    iv.setLayoutY(500);
    //iv1.setFitHeight(250);
    iv1.setPreserveRatio(true);

    Label l2 = new Label("      Python");
        l2.setFont(new Font(30));
        l2.setAlignment(Pos.CENTER);
        l2.setPrefHeight(400);

        

        VBox vb = new VBox(l1,l2);
        //vb.setAlignment(Pos.CENTER);

        HBox hb = new HBox(20,iv,vb);
        hb.setPrefHeight(200);
        hb.setPrefWidth(400);
        hb.setStyle("-fx-background-color:AQUA");

        Group gr = new Group(hb);

        Scene imgsc = new Scene(gr,1000,1000,Color.TEAL);
 
        imgStage.setScene(imgsc);
        imgStage.show();

    
}
}
