package JavaFiles.Home.ImagePage;

import java.beans.EventHandler;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class MyButton1 extends Application{

    @Override
    public void start(Stage butStage) {

        Font ft = Font.font("Courier new", FontWeight.BOLD, 18);

        butStage.setTitle("Button");
        butStage.setHeight(1000);
        butStage.setWidth(1000);
        butStage.setResizable(true);
      butStage.getIcons().add(new Image("assets/image/oo.jpeg")); 

            
            Label l1 = new Label("Core2Web.in");
           l1.setPadding(new Insets(100,0,100,0));
           l1.setFont(new Font(30));
           l1.setFont(ft);
            
            
            Button bt = new Button("Core2Web Super-x");
            bt.setStyle("-fx-background-color:blue");
            bt.setPrefWidth(200);
            bt.setFont(ft);


            bt.setOnAction(new javafx.event.EventHandler<ActionEvent> () {

                    public void handle(ActionEvent Event) {
                        System.out.println("Super-x 2024");
                        bt.setStyle("-fx-background-color:GREEN");

                    }

            });

            Button bt1 = new Button("Core2Web JAVA");
            bt1.setStyle("-fx-background-color:blue");
            bt1.setPrefWidth(200);
            bt1.setFont(ft);

            bt1.setOnAction(new javafx.event.EventHandler<ActionEvent> () {

                    public void handle(ActionEvent Event) {
                        System.out.println("JAVA 2024");
                        bt1.setStyle("-fx-background-color:GREEN");

                    }

            });

            Button bt2 = new Button("Core2Web DSA");
            bt2.setStyle("-fx-background-color:blue");
            bt2.setPrefWidth(200);
            bt2.setFont(ft);

            bt2.setOnAction(new javafx.event.EventHandler<ActionEvent> () {

                    public void handle(ActionEvent Event) {
                        System.out.println("DSA 2024");
                        bt2.setStyle("-fx-background-color:GREEN");

                       
                    }

            });

            VBox vb = new VBox(20,l1,bt,bt1,bt2);
            vb.setAlignment(Pos.CENTER);
            vb.setPrefHeight(400);
            vb.setPrefWidth(400);
         

            Group gr = new Group(vb);
            gr.setLayoutX(300);
            gr.setLayoutY(300);
            
        
     
      Scene imgsc = new Scene(gr,1000,1000,Color.YELLOW);
 
        butStage.setScene(imgsc);
       butStage.show();

    }

    
    }
    

