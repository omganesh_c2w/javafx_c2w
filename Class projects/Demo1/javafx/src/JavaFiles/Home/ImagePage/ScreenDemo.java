package JavaFiles.Home.ImagePage;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class ScreenDemo extends Application {

    @Override
    public void start(Stage primaryStage) {
        // Create the title label
        Button titleLabel = new Button("Study Material");
        titleLabel.setFont(new Font(24));

        // Create the button to add a file
        Button addFileButton = new Button("Add Your File");
        addFileButton.setFont(new Font(20));

        // Create the arrow button
        Button arrowButton = new Button("⬆");
        arrowButton.setFont(new Font(32));

        // Create the bottom buttons
        Button studyMaterialButton = new Button("Study Material");
        Button assignmentButton = new Button("Assignment");
        Button studentsButton = new Button("Students");
        Button messagesButton = new Button("Messages");

        // Create a VBox for the title and add file button
        VBox topVBox = new VBox(10, titleLabel, addFileButton, arrowButton);
        topVBox.setAlignment(Pos.CENTER);
        topVBox.setPadding(new Insets(20));

        // Create an HBox for the bottom buttons
        HBox bottomHBox = new HBox(10, studyMaterialButton, assignmentButton, studentsButton, messagesButton);
        bottomHBox.setAlignment(Pos.CENTER);
        bottomHBox.setPadding(new Insets(20));

        // Create a BorderPane to hold everything
        BorderPane root = new BorderPane();
        root.setTop(topVBox);
        root.setBottom(bottomHBox);

        // Set the scene
        Scene scene = new Scene(root, 600, 400);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Study Material");
        primaryStage.show();
    }

   
}