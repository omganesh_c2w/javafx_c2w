import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class App extends Application {

    public static void main(String[] args) throws Exception {
        launch(args);
        System.out.println("Hello, World!");
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Label text = new Label("Hello OD");

            primaryStage.setTitle("Hello c2w");

            StackPane root = new StackPane();
            root.getChildren().add(text);

            Scene scene = new Scene(root,800,800);

            primaryStage.setScene(scene);
            primaryStage.show();
    }

}
