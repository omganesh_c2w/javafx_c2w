package com.simple_pojo;

public class Main {
    public static void main(String[] args) {
        System.out.println("Player Info!");

        PlayersData pd = new PlayersData();

        pd.setPlayerName("Rohit Sharma");
        pd.setCountry("India");
        pd.setAge(38);

        System.out.println("Player Name : " +pd.getPlayerName());
        System.out.println("Country Name : "+pd.getCountry());
        System.out.println("Player's Age : "+pd.getAge());
    }
}